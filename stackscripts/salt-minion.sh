#!/bin/bash
# for debian only right now
# uses .deb that I compiled - do you trust me?
# https://github.com/fly/linode-stackscripts/blob/master/stackscripts/salt-minion.sh

SS_RUNDIR=$( builtin cd $(dirname $0) ; pwd -P )

if [ -f ./salt-common.sh ]; then
  source ./salt-common.sh
elif [ $SS_RUNDIR == /root/Stackscript ]; then
  echo "Assuming linode stackscript"
  `eval 'source <ssinclude StackScriptID="6357">'`
fi

if [ -f /etc/apt/sources.list ]; then
  add_backports_sources
  system_update
  install_salt_dependencies

  if [ -f /etc/apt/sources.list.d/salt-voltaire.list ]; then
    aptitude -y install salt-common salt-minion
  else
    import_kbar_salt_repo
    apt-get update
    aptitude -y install salt-common salt-minion
  fi

else
  echo "Your distro isn't supported."
  exit
fi

