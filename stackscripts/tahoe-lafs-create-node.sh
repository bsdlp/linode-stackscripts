#!/bin/bash
# eventually we'll want to make everything one script
# and take advantage of UDF to choose what the script does
# https://github.com/fly/linode-stackscripts/blob/master/stackscripts/tahoe-lafs-create-node.sh

source <ssinclude StackScriptID="6185">	# source tahoe-lafs-library.sh StackScript ID
system_update
install_tahoe_dependencies
tahoe_create_node
