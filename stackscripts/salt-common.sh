#!/bin/bash
# for debian only right now
# uses .deb that I compiled - do you trust me?
# https://github.com/fly/linode-stackscripts/blob/master/stackscripts/salt-common.sh

SALT_SUFFIX=_0.14.0-1_all

function add_backports_sources {
  if [ -f /etc/apt/sources.list ]; then
    echo 'deb http://backports.debian.org/debian-backports squeeze-backports main' > /etc/apt/sources.list.d/backports.list
  else
    echo "Your distro isn't supported."
    exit
  fi
}

function system_update {
  if [ -f /etc/apt/sources.list ]; then
    apt-get update
    apt-get -y install aptitude
    aptitude -y full-upgrade
  else
    echo "Your distro isn't supported."
    exit
  fi
}

function install_salt_dependencies {
  if [ -f /etc/apt/sources.list.d/backports.list ]; then
    aptitude install -y build-essential fakeroot
    aptitude install -y python-argparse python-zmq
    apt-get -y -t squeeze-backports install debhelper python-sphinx
  elif [ -f /etc/apt/sources.list ]; then
    add_backports_sources
  else
    echo "Your distro isn't supported."
    exit
  fi
}

function build_salt_from_git {
  git clone https://github.com/saltstack/salt.git /opt/salt
  cd /opt/salt
  fakeroot debian/rules binary
}

function import_kbar_salt_repo {
  wget -q -O- "http://salt.deb.voltaire.sh/salt-voltaire.key" | apt-key add -
  echo "deb http://salt.deb.voltaire.sh/ squeeze main" > /etc/apt/sources.list.d/salt-voltaire.list
  apt-get update
}

function install_salt_common {
  dpkg -i /opt/salt-common$SALT_SUFFIX.deb
  apt-get -f -y install
}

