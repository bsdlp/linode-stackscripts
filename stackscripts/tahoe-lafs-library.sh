#!/bin/bash
# eventually we'll want to make everything one script
# and use UDF to choose what the script does
# https://github.com/fly/linode-stackscripts/blob/master/stackscripts/tahoe-lafs-library.sh

function system_update {
	if [ -f /etc/apt/sources.list ]; then
		apt-get update
		apt-get -y install aptitude
		aptitude -y full-upgrade
	else
		echo "Your distribution is not supported by this StackScript!"
		exit
	fi
}

function install_tahoe_dependencies {
	if [ -f /etc/apt/sources.list ]; then
		aptitude -y install curl python python-dev python-setuptools build-essential sudo
	else
		echo "Your distribution is not supported by this Stackscript!"
		exit
	fi
	easy_install pip
	pip install wsgiref==0.1.2
	pip install zope.interface==4.0.5
	pip install Twisted==12.1.0
	pip install allmydata-tahoe
}

function introduce_tahoe {
	cd /srv/tahoe/
	sudo -u tahoe tahoe create-introducer /srv/tahoe/.tahoe-introducer
	sudo -u tahoe tahoe start /srv/tahoe/.tahoe-introducer
	introducer_furl=`sudo -u tahoe cat /srv/tahoe/.tahoe-introducer/introducer.furl`
	cd -
}

function tahoe_create_node {
	cd /srv/tahoe
	sudo -u tahoe tahoe create-node
	cd -
}
